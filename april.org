#+SETUPFILE: ~/Documents/roam-notes/style.css
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="style.css" />
#+OPTIONS: toc:nil num:nil
#+OPTIONS: html-postamble:nil
#+title: Rim-to-Rim April


#+BEGIN_EXPORT html
<div class="dropdown">
  <button class="dropdown-btn" aria-haspopup="menu">
    <span>Month</span>
    <span class="arrow"></span>
  </button>
  <ul class="dropdown-content" role="menu">
    <li style="--delay: 1;"><a href="./january.html">January</a></li>
    <li style="--delay: 2;"><a href="./february.html">February</a></li>
    <li style="--delay: 3;"><a href="./march.html">March</a></li>
    <li style="--delay: 4;"><a href="./may.html">May</a></li>
  </ul>
</div>
#+END_EXPORT

* April 7th
|           | Workout                                              | Post Run Activity | Feeling | Pace |
| Sunday    | 85 Minutes                                           |                   |         |      |
| Monday    | Rest Day                                             | Stretching        |         |      |
| Tuesday   | 35 Minutes                                           | Core              |         |      |
| Wednesday | 10 Min Warm-Up, 6 Mile Hard Effort, 10 Min Cool-Down | Stretching        |         |      |
| Thursday  | 40 Minutes                                           | Core              |         |      |
| Friday    | 10 Min Warm-Up, 8 mile Tempo, 10 Min Cool-Down       | Stretching        |         |      |
| Saturday  | Medium-Hard Trail Run                                | Core              |         |      |

#+BEGIN_CENTER
*Weekly Mileage = XXX*
#+END_CENTER
#+BEGIN_EXPORT html
<hr>
#+END_EXPORT

* April 14th
|           | Workout                                              | Post Run Activity | Feeling | Pace |
| Sunday    | 95 Minutes                                           |                   |         |      |
| Monday    | Rest Day                                             | Stretching        |         |      |
| Tuesday   | 35 Minutes                                           | Core              |         |      |
| Wednesday | 10 Min Warm-Up, 6 Mile Hard Effort, 10 Min Cool-Down | Stretching        |         |      |
| Thursday  | 40 Minutes                                           | Core              |         |      |
| Friday    | 10 Min Warm-Up, 8 mile Tempo, 10 Min Cool-Down       | Stretching        |         |      |
| Saturday  | Medium-Hard Trail Run                                | Core              |         |      |

#+BEGIN_CENTER
*Weekly Mileage = XXX*
#+END_CENTER
#+BEGIN_EXPORT html
<hr>
#+END_EXPORT

* April 21st
|           | Workout                                                  | Post Run Activity | Feeling | Pace |
| Sunday    | 95 Minutes                                               |                   |         |      |
| Monday    | Rest Day                                                 | Stretching        |         |      |
| Tuesday   | 35 Minutes                                               | Core              |         |      |
| Wednesday | 10 Min Warm-Up, 4 Miles Hard, 5 Minute Jog, 4 Miles Hard | Stretching        |         |      |
| Thursday  | 40 Minutes                                               | Core              |         |      |
| Friday    | 10 Min Warm-Up, 10 mile Tempo, 10 Min Cool-Down          | Stretching        |         |      |
| Saturday  | Medium-Hard Trail Run                                    | Core              |         |      |

#+BEGIN_CENTER
*Weekly Mileage = XXX*
#+END_CENTER
#+BEGIN_EXPORT html
<hr>
#+END_EXPORT
* April 28th
|           | Workout                                                  | Post Run Activity | Feeling | Pace |
| Sunday    | 95 Minutes                                               |                   |         |      |
| Monday    | Rest Day                                                 | Stretching        |         |      |
| Tuesday   | 35 Minutes                                               | Core              |         |      |
| Wednesday | 10 Min Warm-Up, 4 Miles Hard, 5 Minute Jog, 4 Miles Hard | Stretching        |         |      |
| Thursday  | 40 Minutes                                               | Core              |         |      |
| Friday    | 10 Min Warm-Up, 10 mile Tempo, 10 Min Cool-Down          | Stretching        |         |      |
| Saturday  | Medium-Hard Trail Run                                    | Core              |         |      |

#+BEGIN_CENTER
*Weekly Mileage = XXX*
#+END_CENTER
