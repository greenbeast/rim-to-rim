#+SETUPFILE: ~/Documents/roam-notes/style.css
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="style.css" />
#+OPTIONS: toc:nil num:nil
#+OPTIONS: html-postamble:nil
#+title: Rim-to-Rim February


#+BEGIN_EXPORT html
<div class="dropdown">
  <button class="dropdown-btn" aria-haspopup="menu">
    <span>Month</span>
    <span class="arrow"></span>
  </button>
  <ul class="dropdown-content" role="menu">
    <li style="--delay: 1;"><a href="./january.html">January</a></li>
    <li style="--delay: 2;"><a href="./march.html">March</a></li>
    <li style="--delay: 3;"><a href="./april.html">April</a></li>
    <li style="--delay: 4;"><a href="./may.html">May</a></li>
  </ul>
</div>
#+END_EXPORT


* February 4th
|           | Workout                                        | Post Run Activity | Feeling | Pace |
| Sunday    | 50 minutes                                     |                   |         |      |
| Monday    | Rest Day                                       | Stretching        |         |      |
| Tuesday   | 30 Minutes                                     | Core              |         |      |
| Wednesday | 10 Min Warm-Up, 10 Min Tempo, 10 Min Cool-Down | Stretching        |         |      |
| Thursday  | 35 Minutes                                     | Core              |         |      |
| Friday    | 10 Min Warm-Up, 10 Min Tempo, 10 Min Cool-Down | Stretching        |         |      |
| Saturday  | 30 Minutes                                     | Core              |         |      |

#+BEGIN_CENTER
*Weekly Mileage = XXX*
#+END_CENTER
#+BEGIN_EXPORT html
<hr>
#+END_EXPORT


* February 11th
|           | Workout                                        | Post Run Activity | Feeling | Pace |
| Sunday    | 55 minutes                                     |                   |         |      |
| Monday    | Rest Day                                       | Stretching        |         |      |
| Tuesday   | 35 Minutes                                     | Core              |         |      |
| Wednesday | 10 Min Warm-Up, 10 Min Tempo, 10 Min Cool-Down | Stretching        |         |      |
| Thursday  | 40 Minutes                                     | Core              |         |      |
| Friday    | 10 Min Warm-Up, 10 Min Tempo, 10 Min Cool-Down | Stretching        |         |      |
| Saturday  | 30 Minutes                                     | Core              |         |      |


#+BEGIN_CENTER
*Weekly Mileage = XXX*
#+END_CENTER
#+BEGIN_EXPORT html
<hr>
#+END_EXPORT

* February 18th
|           | Workout                                        | Post Run Activity | Feeling | Pace |
| Sunday    | 55 minutes                                     |                   |         |      |
| Monday    | Rest Day                                       | Stretching        |         |      |
| Tuesday   | 35 Minutes                                     | Core              |         |      |
| Wednesday | 10 Min Warm-Up, 15 Min Tempo, 10 Min Cool-Down | Stretching        |         |      |
| Thursday  | 40 Minutes                                     | Core              |         |      |
| Friday    | 10 Min Warm-Up, 15 Min Tempo, 10 Min Cool-Down | Stretching        |         |      |
| Saturday  | 30 Minutes                                     | Core              |         |      |

#+BEGIN_CENTER
*Weekly Mileage = XXX*
#+END_CENTER
#+BEGIN_EXPORT html
<hr>
#+END_EXPORT


* February 25th
|           | Workout                                        | Post Run Activity | Feeling | Pace |
| Sunday    | 65 Minutes                                     |                   |         |      |
| Monday    | Rest Day                                       | Stretching        |         |      |
| Tuesday   | 35 Minutes                                     | Core              |         |      |
| Wednesday | 10 Min Warm-Up, 15 Min Tempo, 10 Min Cool-Down | Stretching        |         |      |
| Thursday  | 40 Minutes                                     | Core              |         |      |
| Friday    | 10 Min Warm-Up, 15 Min Tempo, 10 Min Cool-Down | Stretching        |         |      |
| Saturday  | 35 Minutes                                     | Core              |         |      |

#+BEGIN_CENTER
*Weekly Mileage = XXX*
#+END_CENTER
